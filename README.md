# Containers/VSCode integration tests

[Node.js][node-image] `current-slim` based [image][container-registry] suitable
for running VSCode integration tests in GitLab CI.

## Installed packages

This image installs `xvfb` and its dependencies listed in the [VSCode CI
documentation][vscode-docs].

## Available image tags

Only the [latest][container-registry] tag is available for this image.

## Example usage in GitLab CI

Example of simple usage:

```yaml
test:
  image: registry.gitlab.com/mareksapota-public/containers/vscode-integration-tests:latest

  script:
    - xvfb-run -a npm run test
```

A more full featured example including `npm` package caching and VSCode test
runtime caching (`.vscode-test`):

```yaml
.cache: &npm-cache
  key:
    files:
      - package-lock.json
  paths:
    - .npm/

.npm-job:
  before_script:
    - npm ci --cache .npm --prefer-offline

build:
  extends: .npm-job
  stage: build
  cache:
    - <<: *npm-cache
  script:
    - npm run build

test:
  image: registry.gitlab.com/mareksapota-public/containers/vscode-integration-tests:latest
  extends: .npm-job
  stage: test
  cache:
    - <<: *npm-cache
    - key: ${CI_JOB_NAME}-${CI_COMMIT_REF_SLUG}
      paths:
        - .vscode-test/
  script:
    - xvfb-run -a npm run test
```

## Updates

This image auto updates on a [weekly schedule][ci-schedule] based on the latest
available `node-current:slim` [image][node-image].

# Notice

This image is unofficial and not associated with neither the [VSCode][vscode]
project nor the [Node.js][nodejs] project.

As with many OCI images, this image contains software under various licenses
(such as any packages from the base distribution and/or indirect dependencies of
the primary software being contained).  As for any pre-built image usage, it is
the image user's responsibility to ensure that any use of this image complies
with any relevant licenses for all software contained within.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[nodejs]: https://nodejs.org/
[node-image]: https://hub.docker.com/_/node/
[vscode-docs]: https://code.visualstudio.com/api/working-with-extensions/continuous-integration#gitlab-ci
[ci-schedule]: https://gitlab.com/mareksapota-public/containers/vscode-integration-tests/-/pipeline_schedules
[container-registry]: https://gitlab.com/mareksapota-public/containers/vscode-integration-tests/container_registry/3638273
[vscode]: https://code.visualstudio.com/
